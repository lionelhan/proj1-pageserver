# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon. 

  
  ```
  ## Author: Qi Han, qhan@uoregon.edu ##
  ```
  
* First, we need to copy the credentials-skel.ini file to credentials.ini, then edit it to contain correct information such as our name, repo port and docroote
  
* Second, we need to revise the pageserver.py file in the pageserver folder according to the requirements in `README.md` file to build up a pageserver

* Next, revise the `README.md file`. We need to erase contents that no longer relevant and add personal informations and what we did for this project

* Last, we need to turn the `credentials.ini` file in Canvas 

